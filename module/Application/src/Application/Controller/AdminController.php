<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Form\MakeForm;
use Application\Form\ModelForm;
use Application\Form\AdForm;
use Application\Entity\Make;
use Application\Entity\Model;
use Application\Entity\Ad;

class AdminController extends AbstractActionController
{
    protected $_objectManager;
     
    public function indexAction()
    {
        die($this->zfcUserAuthentication()->getIdentity());
        
        checkAccess();
        return new ViewModel();
    }
    
    public function listMakeAction()
    {
        $this->checkAllowed('adminPanel', 'list-make');
        $makes = $this->getObjectManager()->getRepository('\Application\Entity\Make')->findAll();
        return new ViewModel(array('makes' => $makes));
    }
	
	public function addMakeAction(){
	   $this->checkAllowed('adminPanel', 'add-make');
		$form = new MakeForm();
		$request = $this->getRequest();
		if ($request->isPost()) {
			//procesam formularul
			$make = new Make();
            // Make certain to merge the files info!
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
			$form->setData($post);
			if ($form->isValid()) {
                $make->setName($form->get('name')->getValue());
                $temp_path = $form->getData()['logo']['tmp_name'];
                $file_name = substr($temp_path, strpos($temp_path,'logo_'));
                $make->setLogo($file_name);
                $this->getObjectManager()->persist($make);
                $this->getObjectManager()->flush();
				return $this->redirect()->toRoute('admin', array('action'=>'list-make'));
            }else{
                
            }
		}
		//afisam formularul 
		return new ViewModel(array('form' => $form));
	}
    
    public function updateMakeAction(){
        $this->checkAllowed('adminPanel', 'update-make');
        $id = (int) $this->params()->fromRoute('id', 0);
        $make = $this->getObjectManager()->find('\Application\Entity\Make', $id);
        
        $form = new MakeForm();
        $form->isEdit(true);
        $request = $this->getRequest();
        if ($request->isPost()) {
            //procesam formularul
            // Make certain to merge the files info!
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $make->setName($form->get('name')->getValue());
                $temp_path = $form->getData()['logo']['tmp_name'];
                if($temp_path){
                    //unlink old file
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/uploads/images/'. $make->getLogo());
                    $file_name = substr($temp_path, strpos($temp_path,'logo_'));
                    $make->setLogo($file_name);
                }
                $this->getObjectManager()->persist($make);
                $this->getObjectManager()->flush();
                return $this->redirect()->toRoute('admin', array('action'=>'list-make'));
            }else{
                
            }
        }else{
            $form->setData(array('id'=> $make->getId(), 'name' => $make->getName()));
        }
        //afisam formularul 
        return new ViewModel(array('form' => $form));
    }

    public function deleteMakeAction(){
        //delete
        $this->checkAllowed('adminPanel', 'delete-make');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $id  = (int)$request->getPost()['id'];
            $make = $this->getObjectManager()->find('\Application\Entity\Make', $id);
            unlink($_SERVER['DOCUMENT_ROOT'] . '/uploads/images/'. $make->getLogo());
            $this->getObjectManager()->remove($make);
            $this->getObjectManager()->flush();
            
        }
        //redirect to list-make action
        $this->redirect()->toRoute('admin');
    }
    
    public function listModelAction(){
        $this->checkAllowed('adminPanel', 'list-model');
        $makes = $this->getObjectManager()->getRepository('\Application\Entity\Make')->findAll();
        return new ViewModel(array('makes' => $makes));
    }
    
    public function addModelAction(){
        $this->checkAllowed('adminPanel', 'add-model');
        $form = new ModelForm($this->buildMakesArray());
        
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            //procesam formularul
            $model= new Model();
            
            // Make certain to merge the files info!
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $model->setName($form->get('name')->getValue());
                $idMake  = (int)$request->getPost()['make'];
                $make = $this->getObjectManager()->find('\Application\Entity\Make', $idMake);
                $model->setMake($make);
                $this->getObjectManager()->persist($model);
                $this->getObjectManager()->flush();
                return $this->redirect()->toRoute('admin', array('action'=>'list-model'));
            }else{
                
            }
        }
        //afisam formularul 
        return new ViewModel(array('form' => $form));
    }

    public function updateModelAction(){
        $this->checkAllowed('adminPanel', 'update-model');
        $id = (int) $this->params()->fromRoute('id', 0);
        $model = $this->getObjectManager()->find('\Application\Entity\Model', $id);
        
        $form = new ModelForm($this->buildMakesArray());
        $request = $this->getRequest();
        if ($request->isPost()) {
            //procesam formularul
            // Make certain to merge the files info!
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $model->setName($form->get('name')->getValue());
                $idMake  = (int)$request->getPost()['make'];
                $make = $this->getObjectManager()->find('\Application\Entity\Make', $idMake);
                $model->setMake($make);
                $this->getObjectManager()->persist($model);
                $this->getObjectManager()->flush();
                return $this->redirect()->toRoute('admin', array('action'=>'list-model'));
            }else{
                
            }
        }else{
            $form->setData(array('id'=> $model->getId(), 'name' => $model->getName(), 'make' => $model->getMake()->getId()));
        }
        //afisam formularul 
        return new ViewModel(array('form' => $form));
    }

    public function deleteModelAction(){
        //delete
        $this->checkAllowed('adminPanel', 'delete-model');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $id  = (int)$request->getPost()['id'];
            $model = $this->getObjectManager()->find('\Application\Entity\Model', $id);
            $this->getObjectManager()->remove($model);
            $this->getObjectManager()->flush();
            
        }
        //redirect to list-make action
        $this->redirect()->toRoute('admin', array('action'=>'list-model'));
    }
    
    public function addAdAction(){
       $this->checkAllowed('adminPanel', 'add-ad');
        $form = new AdForm($this->buildMakesArray());
        $request = $this->getRequest();
        if ($request->isPost()) {
            //procesam formularul
            $ad = new Ad();
            // Make certain to merge the files info!
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            
            $form->setData($post);
            $models = $this->getModelsForMake((int)$post['make']);
            $form->get('model')->setOptions( array('value_options'=>$models));
            

            if ($form->isValid()) {
                $modelId = (int) $form->get('model')->getValue(); 
                $model = $this->getObjectManager()->find('\Application\Entity\Model', $modelId);
                $ad->setModel($model);
                $ad->setText($form->get('text')->getValue());
                $ad->setPrice((int)$form->get('price')->getValue());
                $ad->setCurrency($form->get('currency')->getValue());
                $ad->setState("A");
                $ad->setUser($this->zfcUserAuthentication()->getIdentity());
                
                $temp_path = $form->getData()['image']['tmp_name'];
                $file_name = substr($temp_path, strpos($temp_path,'ad_'));
                $ad->setImage($file_name);
                $this->getObjectManager()->persist($ad);
                $this->getObjectManager()->flush();
                return $this->redirect()->toRoute('admin', array('action'=>'list-ad'));
            }
        }
        //afisam formularul 
        return new ViewModel(array('form' => $form));
    }
    
    public function listAdAction()
    {
    	$this->checkAllowed('adminPanel', 'list-ad');
    	$ads = $this->getObjectManager()->getRepository('\Application\Entity\Ad')->findBy(array("user"=>$this->zfcUserAuthentication()->getIdentity()));
    	return new ViewModel(array('ads' => $ads));
    }
    
    public function deleteAdAction(){
    	//delete
    	$this->checkAllowed('adminPanel', 'delete-ad');
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$id  = (int)$request->getPost()['id'];
    		$ad = $this->getObjectManager()->find('\Application\Entity\Ad', $id);
    		unlink($_SERVER['DOCUMENT_ROOT'] . '/uploads/images/'. $ad->getImage());
    		$this->getObjectManager()->remove($ad);
    		$this->getObjectManager()->flush();
    
    	}
    	//redirect to list-make action
    	$this->redirect()->toRoute('admin', array('action' => 'list-ad'));
    }
    
    public function updateAdAction(){
    	$this->checkAllowed('adminPanel', 'update-ad');
    	$id = (int) $this->params()->fromRoute('id', 0);
    	$ad = $this->getObjectManager()->find('\Application\Entity\Ad', $id);
    
    	$form = new AdForm($this->buildMakesArray());
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		//procesam formularul
    		// Make certain to merge the files info!
    		$post = array_merge_recursive(
    				$request->getPost()->toArray(),
    				$request->getFiles()->toArray()
    		);
    		$form->setData($post);
    		$models = $this->getModelsForMake((int)$post['make']);
    		$form->get('model')->setOptions( array('value_options'=>$models));
    		
    		if ($form->isValid()) {
    			$modelId = (int) $form->get('model')->getValue();
    			$model = $this->getObjectManager()->find('\Application\Entity\Model', $modelId);
    			$ad->setModel($model);
    			$ad->setText($form->get('text')->getValue());
    			$ad->setPrice((int)$form->get('price')->getValue());
    			$ad->setCurrency($form->get('currency')->getValue());
    			$ad->setState("A");
    			$ad->setUser($this->zfcUserAuthentication()->getIdentity());
    			$temp_path = $form->getData()['image']['tmp_name'];
    			if($temp_path){
    				//unlink old file
    				unlink($_SERVER['DOCUMENT_ROOT'] . '/uploads/images/'. $ad->getImage());
    				$file_name = substr($temp_path, strpos($temp_path,'ad_'));
    				$ad->setImage($file_name);
    			}
    			$this->getObjectManager()->persist($ad);
    			$this->getObjectManager()->flush();
    			return $this->redirect()->toRoute('admin', array('action'=>'list-ad'));
    		}else{
    
    		}
    	}else{
    		$form->setData(
    				array('id'=> $ad->getId(), 
    						'make' => $ad->getModel()->getMake()->getId(), 
    						'price'=> $ad->getPrice(),
    						'currency' => $ad->getCurrency(),
    						'text' => $ad->getText()
    				));
    		$form->get('model')->setAttribute('data-value', $ad->getModel()->getId());
    	}
    	//afisam formularul
    	return new ViewModel(array('form' => $form));
    }
    
    private function buildMakesArray(){
        $makes = $this->getObjectManager()->getRepository('\Application\Entity\Make')->findAll();
        $makesArray = array();
        foreach($makes as $make){
            $makesArray[$make->getId()] = $make->getName();
        }
        return $makesArray;
    }
    
    public function getModelsAction(){
        $request = $this->getRequest();
        $id = (int) $this->params()->fromRoute('id', 0);
        $models = $this->getModelsForMake($id);
       return new JsonModel(array(
            "options" => $models
       ));
    }
    
    protected function getModelsForMake($id){
    	$make = $this->getObjectManager()->find('\Application\Entity\Make', $id);
    	$models = array();
    	if($make != null){
    		foreach($make->getModels() as $dbModel){
    			$model = array();
    			$model["text"] = $dbModel->getName();
    			$model["value"] = $dbModel->getId();
    			array_push($models, $model);
    		}
    	}
    	return $models;
    }
    
    protected function getObjectManager()
    {
        if (!$this->_objectManager) {
            $this->_objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }

        return $this->_objectManager;
    }
    
    private function checkAllowed($resource, $privilege){
        if (!$this->zfcUserAuthentication()->hasIdentity() ) {
             $this->redirect()->toRoute('zfcuser/login', array('redirect'=>'admin'));
        }
        
        if ( !$this->isAllowed($resource, $privilege)) {
            // $this->redirect()->toRoute('application');
             throw new \BjyAuthorize\Exception\UnAuthorizedException('Not allowed');
        }
    }
}
