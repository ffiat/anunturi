<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
	protected $_objectManager;
	
    public function indexAction()
    {
    	$ads = $this->getAds(6);
        return new ViewModel(array("ads"=>$ads));
    }
    
    public function listAdsAction(){
    	$ads = $this->getAds();
    	return new ViewModel(array("ads"=>$ads));
    }
    
    private function getAds($limit=null, $offset=0){
    	$ads = $this->getObjectManager()->getRepository('\Application\Entity\Ad')->findBy(array(), array('creationDate' => 'DESC'), $limit, $offset);
    	return $ads;
    }
    
    protected function getObjectManager()
    {
    	if (!$this->_objectManager) {
    		$this->_objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    	}
    
    	return $this->_objectManager;
    }
}
