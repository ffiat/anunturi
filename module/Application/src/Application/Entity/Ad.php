<?php

namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
/** 
 * @ORM\Entity 
 * @ORM\Table(name="anunturi")
 * */
class Ad {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;

    /** @ORM\Column(name="text", type="string", length=145) */
    protected $text;
    
    /** @ORM\Column(name="pret", type="integer") */
    protected $price;
    
    /** @ORM\Column(name="moneda", type="string", length=3) */
    protected $currency;
    
    /** @ORM\Column(name="poza", type="string", length=25) */
    protected $image;
    
     /**
     * @ORM\ManyToOne(targetEntity="Model", inversedBy="ads")
     * @ORM\JoinColumn(name="model", referencedColumnName="id")
     **/
    protected $model;
    
     /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ads")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     **/
    protected $user;
    
    /** @ORM\Column(name="data", type="datetime") */
    protected $creationDate;
    
    /** @ORM\Column(name="status", type="string", length=1) */
    protected $state;
    
    public function __construct()
    {
        $this->models = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel(Model $value=null)
    {
        $this->model = $value;
    }
    
    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $value=null)
    {
        $this->user = $value;
    }
    
    public function getText()
    {
        return $this->text;
    }

    public function setText($value)
    {
        $this->text = $value;
    }
    
    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($value)
    {
        $this->price = $value;
    }
    
    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency($value)
    {
        $this->currency = $value;
    }
    
    public function getImage()
    {
        return $this->image;
    }

    public function setImage($value)
    {
        $this->image = $value;
    }
    
    public function getState()
    {
    	return $this->state;
    }
    
    public function setState($value)
    {
    	$this->state = $value;
    }
    
    public function getCreationDate()
    {
    	return $this->creationDate;
    }
    
    public function getArrayCopy()
    {
    return get_object_vars($this);
    }
}
