<?php

namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
/** 
 * @ORM\Entity 
 * @ORM\Table(name="marci")
 * */
class Make {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;

    /** @ORM\Column(name="nume", type="string", length=20) */
    protected $name;
    
    /** @ORM\Column(name="sigla", type="string", length=25) */
    protected $logo;
    
    /**
     *  @ORM\OneToMany(targetEntity="Model", mappedBy="make", cascade={"persist"})
     */
    protected $models;
    
    public function __construct()
    {
        $this->models = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
    }
    
    public function getLogo()
    {
        return $this->logo;
    }

    public function setLogo($value)
    {
        $this->logo = $value;
    }
    
    public function getModels(){
        return $this->models;
    }
    
    public function getArrayCopy()
    {
    return get_object_vars($this);
    }
}
