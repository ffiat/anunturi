<?php

namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
/** 
 * @ORM\Entity 
 * @ORM\Table(name="modele")
 * */
class Model {
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;

    /** @ORM\Column(name="nume", type="string", length=20) */
    protected $name;
    
    /** 
     * 
     * 
     * @ORM\ManyToOne(targetEntity="Make", inversedBy="models")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="marca", referencedColumnName="id", unique=false, nullable=false)
     * })
     * 
     * 
     */
    protected $make;
    
    /**
     * @ORM\OneToMany(targetEntity="Ad", mappedBy="model")
     **/
    private $ads;
    
    public function __construct() {
        $this->ads = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
    }
    
    public function getMake()
    {
        return $this->make;
    }

    public function setMake(Make $value=null)
    {
        $this->make = $value;
    }
    
     public function getAds(){
        return $this->ads;
    }
    
    public function getArrayCopy()
    {
    return get_object_vars($this);
    }
}
