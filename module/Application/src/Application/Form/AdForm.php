<?php
namespace Application\Form;

use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter;
use Zend\Filter;

class AdForm extends Form implements InputFilterProviderInterface{
    public function __construct($makes)
    {
        parent::__construct();
        
        $this -> setAttribute('method', 'post');
        $this -> setAttribute('class', 'form-horizontal');

        $id = new Element\Hidden();
        $id -> setAttribute('name', 'id');
        $id -> setAttribute('id', 'id');
        $this->add($id);
        
        $make = new Element\Select('make');
        $make -> setLabel('Make');
        $make -> setAttribute('class', 'form-control');
        $make -> setAttribute('name', 'make');
        $make -> setAttribute('id', 'make');
        $make -> setLabelAttributes(array('class' => 'col-sm-2 control-label'));
        $make->setValueOptions($makes);
        $this->add($make);
         
        $model = new Element\Select('model');
        $model -> setLabel('Model');
        $model -> setAttribute('class', 'form-control');
        $model -> setAttribute('name', 'model');
        $model -> setAttribute('id', 'model');
        $model -> setLabelAttributes(array('class' => 'col-sm-2 control-label'));
        //$model -> setDisableInArrayValidator(true);
        $this->add($model);
        
        $text = new Element\Textarea('text');
        $text -> setLabel('Details');
        $text -> setAttribute('class', 'form-control');
        $text -> setAttribute('name', 'text');
        $text -> setAttribute('id', 'text');
        $text -> setLabelAttributes(array('class' => 'col-sm-2 control-label'));
        $this->add($text);
        
        $price = new Element\Number('price');
        $price -> setLabel('Price');
        $price -> setAttribute('class', 'form-control');
        $price -> setAttribute('name', 'price');
        $price -> setAttribute('id', 'price');
        $price -> setLabelAttributes(array('class' => 'col-sm-2 control-label'));
        $this->add($price);
        
        $currency = new Element\Select('currency');
        $currency -> setAttribute('class', 'form-control');
        $currency -> setAttribute('name', 'currency');
        $currency -> setAttribute('id', 'currency');
        $currency->setValueOptions(array("RON"=>"RON", "EUR"=>"EUR", "USD"=>"USD"));
        $this->add($currency);
        
        $image = new Element\File();
        $image -> setAttribute('name', 'image');
        $image -> setAttribute('id', 'image');
        $image -> setAttribute('class', 'form-control');
        $image -> setLabel('Image');
        $image -> setLabelAttributes(array('class' => 'col-sm-2 control-label'));
        // $logo -> setOption('required',true);
        // $logo -> setOption('allowEmpty',false);
        $this->add($image);
        
        $inputFilter = new InputFilter\InputFilter();
        $fileInput = new InputFilter\FileInput('image');
        $fileInput->setRequired(true);
        $fileInput->getFilterChain()->attach(new Filter\File\RenameUpload(array(
         'target' => $_SERVER['DOCUMENT_ROOT'] . '/uploads/images/ad.jpg',
         'randomize' => true,
        )));
        $inputFilter->add($fileInput);

        $this->setInputFilter($inputFilter);
        
        $this->add(new Element\Csrf('security'));
        $this->add(array(
            'name' => 'send',
            'type'  => 'Submit',
            'class' => 'btn btn-default',
            'attributes' => array(
                'value' => 'Submit',
            ),
        ));
    }

    public function setMakes($makes)
    {
        $this->makes = $makes;
    }
    
    public function getInputFilterSpecification()
     {
         
         return array(
             'model' => array(
                 'required' => true,
                 'allow_empty' => false,
             ),
             'make' => array(
                 'required' => true,
                 'allow_empty' => false,
             ),
         );
     }
}
