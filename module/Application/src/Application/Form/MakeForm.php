<?php
namespace Application\Form;

use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter;
use Zend\Filter;

class MakeForm extends Form implements InputFilterProviderInterface
{
    protected $isEdit = false;
    public function __construct()
    {

        parent::__construct();
		
		$this -> setAttribute('method', 'post');
		$this -> setAttribute('class', 'form-horizontal');

        $id = new Element\Hidden();
        $id -> setAttribute('name', 'id');
        $id -> setAttribute('id', 'id');
        $this->add($id);
        
        $name = new Element\Text();
        $name -> setAttribute('name', 'name');
        $name -> setAttribute('id', 'name');
        $name -> setAttribute('class', 'form-control');
        $name -> setLabel('Make');
        $name -> setLabelAttributes(array('class' => 'col-sm-2 control-label'));
        // $name -> setOption('required', true);
        // $name -> setOption('allowEmpty',false);
        $this->add($name);
        
        $logo = new Element\File();
        $logo -> setAttribute('name', 'logo');
        $logo -> setAttribute('id', 'logo');
        $logo -> setAttribute('class', 'form-control');
        $logo -> setLabel('Logo');
        $logo -> setLabelAttributes(array('class' => 'col-sm-2 control-label'));
        // $logo -> setOption('required',true);
        // $logo -> setOption('allowEmpty',false);
        $this->add($logo);
        
        $inputFilter = new InputFilter\InputFilter();
        $fileInput = new InputFilter\FileInput('logo');
        $fileInput->setRequired(true);
        $fileInput->getFilterChain()->attach(new Filter\File\RenameUpload(array(
         'target' => $_SERVER['DOCUMENT_ROOT'] . '/uploads/images/logo.jpg',
         'randomize' => true,
        )));
        $inputFilter->add($fileInput);

        $this->setInputFilter($inputFilter);
        

        $this->add(new Element\Csrf('security'));
        $this->add(array(
            'name' => 'send',
            'type'  => 'Submit',
			'class' => 'btn btn-default',
            'attributes' => array(
                'value' => 'Submit',
            ),
        ));
        
    }

    public function isEdit($isEdit){
        $this->isEdit = $isEdit;
    }
    
    public function getInputFilterSpecification()
     {
         if($this->isEdit){
             return array(
                 'name' => array(
                     'required' => true,
                     'allow_empty' => false,
                 ),
                 'logo' => array(
                     'required' => false,
                     'allow_empty' => true,
                 ),
             );
         }else{
             return array(
             'name' => array(
                 'required' => true,
                 'allow_empty' => false,
             ),
             'logo' => array(
                 'required' => true,
                 'allow_empty' => false,
             ),
         );
         }
     }
}