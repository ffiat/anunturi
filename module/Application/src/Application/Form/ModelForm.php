<?php
namespace Application\Form;

use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter;
use Zend\Filter;

class ModelForm extends Form implements InputFilterProviderInterface{
    public function __construct($makes)
    {
        parent::__construct();
        
        $this -> setAttribute('method', 'post');
        $this -> setAttribute('class', 'form-horizontal');

        $id = new Element\Hidden();
        $id -> setAttribute('name', 'id');
        $id -> setAttribute('id', 'id');
        $this->add($id);
        
        $name = new Element\Text();
        $name -> setAttribute('name', 'name');
        $name -> setAttribute('id', 'name');
        $name -> setAttribute('class', 'form-control');
        $name -> setLabel('Model');
        $name -> setLabelAttributes(array('class' => 'col-sm-2 control-label'));
        // $name -> setOption('required', true);
        // $name -> setOption('allowEmpty',false);
        $this->add($name);
        
        $make = new Element\Select('make');
        $make -> setLabel('Make');
        $make -> setAttribute('class', 'form-control');
        $make -> setAttribute('name', 'make');
        $make -> setAttribute('id', 'make');
        $make -> setLabelAttributes(array('class' => 'col-sm-2 control-label'));
        $make->setValueOptions($makes);
         $this->add($make);
        
        $this->add(new Element\Csrf('security'));
        $this->add(array(
            'name' => 'send',
            'type'  => 'Submit',
            'class' => 'btn btn-default',
            'attributes' => array(
                'value' => 'Submit',
            ),
        ));
    }

    public function setMakes($makes)
    {
        $this->makes = $makes;
    }
    
    public function getInputFilterSpecification()
     {
         
         return array(
             'name' => array(
                 'required' => true,
                 'allow_empty' => false,
             ),
             'make' => array(
                 'required' => true,
                 'allow_empty' => false,
             ),
         );
     }
}
