<?php
    if(!isset($activeLink) ){
        $activeLink="none-active";
    }
?>
<?php if($this->isAllowed("adminPanel", "add-make")):?>
<?php if("add-make"==$activeLink):?>
    <li class="active"><a href="#">Add make</a></li>
<?php else: ?>
    <li><a href="<?php echo $this->url("admin", array("action" => "add-make"))?>">Add make</a></li>                
<?php endif;?>
<?php endif;?>

<?php if($this->isAllowed("adminPanel", "list-make")):?>
<?php if("list-make"==$activeLink):?>
    <li class="active"><a href="#">List makes</a></li>
<?php else: ?>
    <li><a href="<?php echo $this->url("admin", array("action" => "list-make"))?>">List makes</a></li>                
<?php endif;?>
<?php endif;?>

<?php if($this->isAllowed("adminPanel", "add-model")):?>
<?php if("add-model"==$activeLink):?>
    <li class="active"><a href="#">Add model</a></li>
<?php else: ?>
    <li><a href="<?php echo $this->url("admin", array("action" => "add-model"))?>">Add model</a></li>                
<?php endif;?>
<?php endif;?>

<?php if($this->isAllowed("adminPanel", "list-model")):?>
<?php if("list-model"==$activeLink):?>
    <li class="active"><a href="#">List models</a></li>
<?php else: ?>
    <li><a href="<?php echo $this->url("admin", array("action" => "list-model"))?>">List models</a></li>                
<?php endif;?>
<?php endif;?>

<?php if($this->isAllowed("adminPanel", "add-ad")):?>
<?php if("add-ad"==$activeLink):?>
    <li class="active"><a href="#">Add ad</a></li>
<?php else: ?>
    <li><a href="<?php echo $this->url("admin", array("action" => "add-ad"))?>">Add ad</a></li>                
<?php endif;?>
<?php endif;?>

<?php if($this->isAllowed("adminPanel", "list-ad")):?>
<?php if("list-ad"==$activeLink):?>
    <li class="active"><a href="#">List ad</a></li>
<?php else: ?>
    <li><a href="<?php echo $this->url("admin", array("action" => "list-ad"))?>">List ad</a></li>                
<?php endif;?>
<?php endif;?>