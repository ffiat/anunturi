/**
 * 
 */

function populateModel(){
    var marca=$("#make").val();
    var defaultModel = $("#model").attr('data-value');
    $.ajax({url: "/admin/get-models/" + marca,
           type: "POST",
           success: function(result){
				$("#model").find('option').remove();
				$.each(result.options,function(key, val) {
					if(defaultModel == val.value){
						$("#model").append('<option value=' + val.value + ' selected = "selected">' + val.text + '</option>');
					}else{
						$("#model").append('<option value=' + val.value + '>' + val.text + '</option>');
					}
				});
        }});
}